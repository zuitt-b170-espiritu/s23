
// insert single room
db.rooms.insertOne(
    {
        name: "single",
        accomodates: 2,
        price: 1000,
        decription: "A simple room with all the basic necessities",
        rooms_available: 10,
        isAvailable: false
    }
)


// insert multiple rooms
db.rooms.insertMany(
[
    {
        name: "double",
        accomodates: 3,
        price: 2000,
        decription: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: false
    },
    {
        name: "queen",
        accomodates: 4,
        price: 4000,
        decription: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
    }
]
)


// find double
db.rooms.find(
    {
        name: "double"
    },
    {
        _id: 0
    }
)



// update available rooms of queen
db.rooms.updateOne(
    {
        name: "queen"
    },
    {
        $set: {
            rooms_available: 0
        }
    }
)


// delete with 0 available rooms
db.rooms.deleteMany({rooms_available: 0})