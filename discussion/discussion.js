
// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/
db.users.insert({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    department: "none"
});

// Insert Many
/*
    - Syntax
        - db.collectionName.insertMany([ {objectA}, {objectB} ]]);
*/
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neil.a@gmail.com"
        },
        department: "science"
    },
    {
        firstName: "Neil",
        lastName: "Diamond",
        age: 46,
        contact: {
            phone: "87654321",
            email: "neil.d@gmail.com"
        },
        department: "music"
    }
]);

// [Section] Finding documents (Read)
// Find
/*
    - If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
    - This is based from the order that documents are stored in a collection
    - If a document is not found, the terminal will respond with a blank line
    - Syntax
        - db.collectionName.find();
        - db.collectionName.find({ field: value });
*/

// Finding a single document
// Leaving the search criteria empty will retrieve ALL the documents
db.users.find({query}, {projection});

db.users.find({ firstName: "Stephen" });
db.users.find(
    { 
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76
    }
);

// The "pretty" method allows us to be able to view the documents returned by our terminals in a better format
    // Note that this is mostly applicable in terminals and older versions of robo3t (1.1 and older)
db.users.find({ firstName: "Stephen" }).pretty();

// Finding documents with multiple parameters
/*
    - Syntax
        - db.collectionName.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ lastName: "Armstrong", age: 82 })


// Finding documents with the use of query comparison operator
db.users.find(
    {
        age: { $gt: 20 }
    }
);

db.users.find(
    {
        age: { $eq: 75 }
    }
);

db.users.find(
    {
        age: 75
    }
);

db.users.find(
    {
        lastName: { $in: ["Doe", "Armstrong", "Hawking"] }
    }
)

// Finding documents with the use of query logical operator

    //using and logical operator
db.users.find(
    {
        $and: [
            { firstName: "Jane" },
            { lastName: "Doe" }
        ]
    }
)

db.users.find(
    {
        firstName: "Jane",
        lastName: "Doe"
    }
)

    // using or logical operator
db.users.find(
    {
        $or: [
            { firstName: "Stephen" },
            { lastName: "Doe" },
            { age: {$gte: 80} }
        ]
    }
)


    // using regex operator
db.users.find(
    {
        firstName: { $regex: "EI", $options: "i" }
    }
)


// Finding documents with the use of query and field projection
    // specify 1 or 0 if you wish to include or exclude the fields of a document
db.users.find(
    {
        _id: ObjectId("624ffc72895b8af0aca5317d")
    },
    {
        firstName: 1,
        age: 1,
        _id: 0
    }
)

// Look for a document that has a firstName of Neil and display only the first name and phone
db.users.find(
    {
        firstName: "Neil"
    },
    {
        firstName: 1,
        contact: {phone: 1},
        _id: 0
    }
)


// Update Operation
    // Update operations modify existing documents in a collection. MongoDB provides the following methods to update documents of a collection

    // db.collections.updateOne({filter}, {update})
    // db.collections.updateMany({filter}, {update})
    // db.collections.replaceOne()

    //we will insert a dummy document to be able to use for update operation
    db.users.insertOne(
        {
            firstName: "Test",
            lastName: "Test",
            age: 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    )

db.users.updateOne(
    {
        firstName: "Test"
    },
    {
        $set: {
            firstName: "Joy",
            lastName: "Pague",
            age: 16,
            contact: {
                phone: "12345678910",
                email: "joy@mail.com"
            },
            department: "none"
        }
    }
)

// update the newly updated document with department to Instructor Department
db.users.updateOne(
    {
        firstName: "Joy"
    },
    {
        $set: {
            department: "Instructor"
        }
    }
)

// remove the specified field using $unset update operator
db.users.updateOne(
    {
        firstName: "Joy"
    },
    {
        $unset: {
            department: "Instructor"
        }
    }
)

// What if I got multiple documents from the filter paramter and I only want to update one document
    //we will insert a dummy documents to be able to use for update operation
    db.users.insertMany(
        [
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            },
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            }
        ]
    )

//using updateOne to update these newly added documents
db.users.updateOne(
    {
        firstName: "Test"
    },
    {
        $set: {
            firstName: "Hannah",
            lastName: "Tapay",
            age: 18,
            contact: {
                phone: "12345678910",
                email: "hannah@mail.com"
            },
            department: "none"
        }
    }
)


// Find all documents with department field and update all documents to HR department
db.users.updateMany( 
    {  
        department: {$exists: true}
 
    },
    { 
       $set:{ 
            department : "HR"
        }
    }
 );



// db.collections.replaceOne({filter}, {replacement})
    // replaces a single document
db.users.replaceOne(
    {
        firstName: "Test"
    },
    {
        firstName: "Johnny",
        lastName: "Cuyno"
    }
)


// Delete Operations
    // Delete operations remove documents from a collection. MongoDB provides the following methods to delete documents of a collection:

    // db.collections.deleteOne({filter})
    // db.collections.deleteMany({filter})

    //we will insert a dummy documents to be able to use for update operation
    db.users.insertOne(
        {
            firstName: "Test",
            lastName: "Test",
            age: 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    )

// to delete an existing single document, use deleteOne method
db.users.deleteOne({firstName: "Test"})


    // inserting dummy documents for deleteMany() method
    db.users.insertMany(
        [
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            },
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            }
        ]
    )

// delete multiple documents
db.users.deleteMany({lastName: "Test"})


// delete multiple documents targetting same field with different values
db.users.deleteMany(
    {
        firstName: { $in: ["Joy", "Hannah", "Johnny"] }
    }
)